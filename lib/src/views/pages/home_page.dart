import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class HomePage extends StatelessWidget {
  final bool isCollapse;

  final double screenHeight, screenWidth;

  final Duration duration;

  final Widget child;

  final Function onMenuTap;

  HomePage(
      {Key key,
      this.isCollapse,
      this.screenHeight,
      this.screenWidth,
      this.duration,
      this.onMenuTap,
      this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedPositioned(
      duration: duration,
      top: isCollapse ? 0 : 0.1 * screenHeight,
      bottom: isCollapse ? 0 : 0.1 * screenHeight,
      left: isCollapse ? 0 : 0.6 * screenWidth,
      right: isCollapse ? 0 : -0.4 * screenWidth,
      child: child, /*HomeBottonNavbar(
        onMenuTap: onMenuTap,
      ),*/
    );
  }
}
