
const double sideBoxMenuDivider = 10;

const double menuFontSize = 20;

const double cardCampaignWidth = 200;

const double cardCampaignValueFontSize = 10;

const double cardCampaignLabelFontSize = 8;

const double cardCampaignTitlePrimaryFontSize = 12;

const double cardCampaignTitleSecondaryFontSize = 10 ;
