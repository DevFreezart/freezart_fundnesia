import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:freezart/src/bloc/state/navigation_sidebar_state.dart';
import 'package:freezart/src/views/constants/color_constant.dart';
import 'package:freezart/src/views/layouts/navbar/home_navbar.dart';
import 'package:freezart/src/views/layouts/tabbars/items/home_home_item.dart';
import 'package:freezart/src/views/layouts/widgets/kyc_alert.dart';

class HomeLayout extends StatefulWidget with NavigationSidebarState{
  final Function onMenuTap;

  HomeLayout({Key key, this.onMenuTap}) : super(key: key);

  @override
  _HomeLayoutState createState() => _HomeLayoutState();
}

class _HomeLayoutState extends State<HomeLayout> {
  int _selectedIndex = 0;

  final double bottomNavbarHeight = 50;

  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  List<Widget> _widgetOptions = <Widget>[
    HomeTabbarItem(),
    Center(
      child: Text(
        'Berita',
        style: optionStyle,
      ),
    ),
    Center(
      child: Text(
        'Simulasi',
        style: optionStyle,
      ),
    ),
    Center(
      child: Text(
        'Akun',
        style: optionStyle,
      ),
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      extendBody: true,
      appBar: AppBar(
        backgroundColor: backgroundColor,
        flexibleSpace: HomeNavbar(
          onMenuTap: widget.onMenuTap,
        ),
      ),
      body: Container(
        color: Color(0xFFE9E9E9),
        child: Stack(
          children: [
            _widgetOptions.elementAt(_selectedIndex),
            // KycAlert(),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(25), topLeft: Radius.circular(25)),
          boxShadow: [
            BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 8),
          ],
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(25.0),
            topRight: Radius.circular(25.0),
          ),
          child: SizedBox(
            height: bottomNavbarHeight,
            child: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.home,size: bottomNavbarHeight * 0.4,),
                  label: 'Beranda',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.text_snippet,size: bottomNavbarHeight * 0.4,),
                  label: 'Berita',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.calculate_outlined,size: bottomNavbarHeight * 0.4,),
                  label: 'Simulasi',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.person_outline,size: bottomNavbarHeight * 0.4,),
                  label: 'Akun',
                )
              ],
              currentIndex: _selectedIndex,
              onTap: _onItemTapped,
              selectedItemColor: backgroundColor,
            ),
          ),
        ),
      ),
    );
  }
}
