import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezart/src/bloc/NavigationSidebarBloc.dart';
import 'package:freezart/src/bloc/state/navigation_sidebar_state.dart';
import 'package:freezart/src/views/constants/color_constant.dart';
import 'package:freezart/src/views/layouts/home_layout.dart';
import 'package:freezart/src/views/layouts/menu_layout.dart';
import 'package:freezart/src/views/pages/home_page.dart';

class MainLayout extends StatefulWidget {
  @override
  _MainLayoutState createState() => _MainLayoutState();
}

class _MainLayoutState extends State<MainLayout> {
  bool isCollapse = true;

  double screenWidth, screenHeight;

  final Duration duration = const Duration(milliseconds: 200);

  void onMenuTap() {
    setState(() {
      isCollapse = !isCollapse;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    screenHeight = size.height;
    screenWidth = size.width;

    return Material(
      color: backgroundColor,
      elevation: 8,
      child: SafeArea(
        child: BlocProvider<NavigationSidebarBloc>(
          create: (context) => NavigationSidebarBloc(onMenuTap: onMenuTap),
          child: Stack(
            children: [
              BlocBuilder<NavigationSidebarBloc,NavigationSidebarState>(
                  builder: (context, NavigationSidebarState navigationState) {
                    return MenuLayout();
                  }
              ),
              HomePage(
                isCollapse: isCollapse,
                screenHeight: screenHeight,
                screenWidth: screenWidth,
                duration: duration,
                onMenuTap: onMenuTap,
                child: BlocBuilder<NavigationSidebarBloc, NavigationSidebarState>(
                    builder: (context, state,) {
                      return state as Widget;
                    }
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
