import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezart/src/bloc/NavigationSidebarBloc.dart';
import 'package:freezart/src/bloc/enums/navigation_sidebar_event.dart';
import 'package:freezart/src/views/constants/size_constant.dart';

class MenuLayout extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.only(left: 16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(
                bottom: sideBoxMenuDivider,
              ),
              child: InkWell(
                child: Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                        right: 5,
                      ),
                      child: Icon(
                        Icons.home,
                        color: Colors.white,
                      ),
                    ),
                    Text(
                      "Beranda",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: menuFontSize,
                      ),
                    ),
                  ],
                ),
                onTap:  () {
                  BlocProvider.of<NavigationSidebarBloc>(context).add(NavigationSidebarEvent.HOME);
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                bottom: sideBoxMenuDivider,
              ),
              child: InkWell(
                child: Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                        right: 5,
                      ),
                      child: Icon(
                        Icons.home,
                        color: Colors.white,
                      ),
                    ),
                    Text(
                      "Hubungi kami",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: menuFontSize,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                bottom: sideBoxMenuDivider,
              ),
              child: InkWell(
                onTap: () {
                  BlocProvider.of<NavigationSidebarBloc>(context).add(NavigationSidebarEvent.FAQ);
                },
                child: Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                        right: 5,
                      ),
                      child: Icon(
                        Icons.home,
                        color: Colors.white,
                      ),
                    ),
                    Text(
                      "Tanya Jawab",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: menuFontSize,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                bottom: sideBoxMenuDivider,
              ),
              child: InkWell(
                child: Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                        right: 5,
                      ),
                      child: Icon(
                        Icons.supervised_user_circle_outlined,
                        color: Colors.white,
                      ),
                    ),
                    Text(
                      "Tentang Kami",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: menuFontSize,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                bottom: sideBoxMenuDivider,
              ),
              child: InkWell(
                child: Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                        right: 5,
                      ),
                      child: Icon(
                        Icons.logout,
                        color: Colors.white,
                      ),
                    ),
                    Text(
                      "Keluar",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: menuFontSize,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );;
  }
}
