import 'package:flutter/material.dart';
import 'package:freezart/src/bloc/state/navigation_sidebar_state.dart';
import 'package:freezart/src/views/constants/color_constant.dart';
import 'package:freezart/src/views/layouts/navbar/home_navbar.dart';

class FaqPage extends StatelessWidget with NavigationSidebarState{

  final Function onMenuTap;

  const FaqPage({Key key, this.onMenuTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      appBar: AppBar(
        backgroundColor: backgroundColor,
        flexibleSpace: HomeNavbar(
          onMenuTap: onMenuTap,
        ),
      ),
      body: Center(
        child: Text('FAQ'),
      ),
    );
  }
}
