 import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:freezart/src/views/constants/color_constant.dart';

class HomeNavbar extends StatefulWidget {

  final Function onMenuTap;

  const HomeNavbar({Key key, this.onMenuTap}) : super(key: key);

  @override
   _HomeNavbarState createState() => _HomeNavbarState();
 }

 class _HomeNavbarState extends State<HomeNavbar> {

   int _notifCount = 1;

   @override
   Widget build(BuildContext context) {
     return Container(
       height: double.infinity,
       padding: const EdgeInsets.only(left: 5, top: 2, bottom: 2),
       child: Row(
         mainAxisSize: MainAxisSize.max,
         mainAxisAlignment: MainAxisAlignment.spaceBetween,
         children: [
           InkWell(
             child: Container(
               child: Row(
                 children: [
                   Icon(
                     Icons.menu,
                     color: Colors.white,
                   ),
                   Padding(
                     padding: EdgeInsets.only(
                       left: 5,
                     ),
                     child: Image.asset('assets/images/logo_white.png',height: 25,),
                   ),
                 ],
               ),
             ),
             onTap: widget.onMenuTap,
           ),
           Container(
             padding: EdgeInsets.only(
               right: 8,
             ),
             child: Row(
               children: [
                 Text(
                   "TKB 100%",
                   style: TextStyle(color: Colors.white, fontSize: 12),
                 ),
                 InkWell(
                   child: Stack(
                     children: <Widget>[
                       Icon(
                         Icons.notifications_none,
                         color: Colors.white,
                       ),
                       Positioned(
                         right: 0,
                         child: Container(
                           padding: EdgeInsets.all(2),
                           decoration: BoxDecoration(
                             border: Border.all(
                               color: Colors.white,
                             ),
                             color: Colors.redAccent,
                             borderRadius: BorderRadius.circular(20),
                           ),
                           constraints: BoxConstraints(
                             minWidth: 14,
                             minHeight: 14,
                           ),
                           child: Text(
                             '$_notifCount',
                             style: TextStyle(
                               color: Colors.white,
                               fontSize: 8,
                             ),
                             textAlign: TextAlign.center,
                           ),
                         ),
                       )
                     ],
                   ),
                   onTap: () {
                     setState(() {
                       _notifCount += 1;
                     });
                   },
                 ),
               ],
             ),
           )
         ],
       ),
     );
   }
 }
