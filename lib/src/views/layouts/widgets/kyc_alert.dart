import 'package:flutter/material.dart';

class KycAlert extends StatelessWidget {

  final double screenHeight;

  const KycAlert({Key key, this.screenHeight}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      padding: const EdgeInsets.only(top: 5, left: 20, right: 20),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.red[500],
          ),
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: Color(0xF0FFA39E),
        ),
        child: Padding(
          padding: EdgeInsets.only(
            top: 10,
            left: 20,
            right: 20,
            bottom: 5,
          ),
          child: InkWell(
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Icon(
                      Icons.warning,
                      color: Color(0xFFFF444B),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Anda belumn menyelesaikan proses KYC",
                          style: TextStyle(
                            fontSize: 12,
                          ),
                        ),
                        Text(
                          "Tap disini untuk menyelesaikan proses KYC",
                          style: TextStyle(
                            fontSize: 10,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      ],
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
