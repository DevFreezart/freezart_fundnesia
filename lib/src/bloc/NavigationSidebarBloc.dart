import 'package:bloc/bloc.dart';
import 'package:freezart/src/bloc/enums/navigation_sidebar_event.dart';
import 'package:freezart/src/bloc/state/navigation_sidebar_state.dart';
import 'package:freezart/src/views/layouts/home_layout.dart';
import 'package:freezart/src/views/pages/faq_page.dart';

class NavigationSidebarBloc extends Bloc<NavigationSidebarEvent, NavigationSidebarState> {

  final Function onMenuTap;

  NavigationSidebarBloc({this.onMenuTap}) : super(HomeLayout(onMenuTap: onMenuTap,));

  @override
  Stream<NavigationSidebarState> mapEventToState(NavigationSidebarEvent event) async* {
      switch(event) {
        case NavigationSidebarEvent.HOME:
        yield HomeLayout(
          onMenuTap: onMenuTap,
        );
        break;
        case NavigationSidebarEvent.FAQ:
        yield FaqPage(
          onMenuTap: onMenuTap,
        );
        break;
        default:
          yield FaqPage(
            onMenuTap: onMenuTap,
          );

      }
  }

}
