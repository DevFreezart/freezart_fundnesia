import 'package:flutter/material.dart';
import 'package:freezart/src/views/layouts/widgets/campaign_list.dart';
import 'package:freezart/src/views/layouts/widgets/home_courusel.dart';

class HomeTabbarItem extends StatelessWidget {

  const HomeTabbarItem(
      {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      physics: ClampingScrollPhysics(),
      child: Container(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 5,
            ),
            HomeCouruselWidget(),
            SizedBox(
              height: 5,
            ),
            Container(
                padding: EdgeInsets.only(
                  left: 10,
                  right: 10,
                ),
                child: Column(
                  children: [
                    Text("Cara Kerja Kami"),
                    SizedBox(
                      height: 5,
                    ),
                    Image.asset('assets/images/cara_kerja_p2p.png'),
                  ],
                )),
            Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CampaignList(title: "Pendanaan Terdanai",),
                  SizedBox(
                    height: 10,
                  ),
                  CampaignList(title: "Pendanaan Berjalan",),
                  SizedBox(
                    height: 60,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
