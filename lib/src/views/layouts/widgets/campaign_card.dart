import 'package:flutter/material.dart';
import 'package:freezart/src/views/constants/color_constant.dart';
import 'package:freezart/src/views/constants/size_constant.dart';

class CampaignCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.all(Radius.circular(20)),
      elevation: 4,
      child: Container(
        width: cardCampaignWidth,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              fit: FlexFit.loose,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                child: Stack(
                  children: [
                    Image.asset(
                      'assets/images/farming.jpeg',
                      fit: BoxFit.fitHeight,
                    ),
                    Positioned(
                      bottom: 0,
                      child: Container(
                        width: cardCampaignWidth,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          color: Color(0xCCFFFFFF),
                        ),
                        padding: const EdgeInsets.only(
                            left: 8, bottom: 8, top: 5, right: 8),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Invoice/PO",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: cardCampaignTitleSecondaryFontSize),
                              ),
                              Text(
                                "Pendanaan Jagung",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: cardCampaignTitlePrimaryFontSize),
                              ),
                            ]),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Flexible(
              fit: FlexFit.loose,
              child: Container(
                padding: const EdgeInsets.all(8),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          "Nilai Pembiayaan",
                          style: TextStyle(
                              fontSize: cardCampaignLabelFontSize,
                              color: Colors.black54),
                        ),
                        Text(
                          "Rp. 701.000.000",
                          style: TextStyle(
                              fontSize: cardCampaignValueFontSize,
                              color: Colors.black),
                        ),
                        Text(
                          "Imbal Hasil p.a.",
                          style: TextStyle(
                              fontSize: cardCampaignLabelFontSize,
                              color: Colors.black54),
                        ),
                        Text(
                          "20,5%",
                          style: TextStyle(
                              fontSize: cardCampaignValueFontSize,
                              color: Colors.black),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          "Tingkat Resiko",
                          style: TextStyle(
                              fontSize: cardCampaignLabelFontSize,
                              color: Colors.black54),
                        ),
                        Text(
                          "B (Risiko Rendah)",
                          style: TextStyle(
                              fontSize: cardCampaignValueFontSize,
                              color: Colors.black),
                        ),
                        Text(
                          "Jangka Waktu",
                          style: TextStyle(
                              fontSize: cardCampaignLabelFontSize,
                              color: Colors.black54),
                        ),
                        Text(
                          "4 Bulan",
                          style: TextStyle(
                              fontSize: cardCampaignValueFontSize,
                              color: Colors.black),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Flexible(
              fit: FlexFit.loose,
              child: Container(
                padding: const EdgeInsets.only(
                  bottom: 8,
                  left: 8,
                  right: 8,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      "Lokasi Proyek",
                      style: TextStyle(
                          fontSize: cardCampaignLabelFontSize,
                          color: Colors.black54),
                    ),
                    Text(
                      "Kota Jakarta Selatan",
                      style: TextStyle(
                          fontSize: cardCampaignValueFontSize,
                          color: Colors.black),
                    ),
                  ],
                ),
              ),
            ),
            Flexible(
              fit: FlexFit.loose,
              child: Container(
                width: double.infinity,
                padding: const EdgeInsets.only(
                  bottom: 8,
                  left: 8,
                  right: 8,
                ),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                      side: BorderSide(color: Colors.red)),
                  onPressed: () {},
                  color: backgroundColor,
                  textColor: Colors.white,
                  child: Text("Danai".toUpperCase(),
                      style: TextStyle(fontSize: 14)),
                )
              ),
            ),
          ],
        ),
      ),
    );
  }
}
