import 'package:flutter/material.dart';
import 'package:freezart/src/views/constants/color_constant.dart';
import 'package:freezart/src/views/layouts/widgets/campaign_card.dart';

class CampaignList extends StatelessWidget {
  final String title;

  const CampaignList({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Padding(
        padding: const EdgeInsets.only(left: 10.0, right: 10.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "$title",
              style: TextStyle(fontSize: 16),
            ),
            SizedBox(
              height: 10,
            ),
            InkWell(
                child: Text(
              "Lihat semua",
              style: TextStyle(fontSize: 12, color: backgroundColor),
            )),
          ],
        ),
      ),
      SizedBox(
        height: 10,
      ),
      SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            for (int i = 0; i < 10; i++)
              Padding(
                padding: const EdgeInsets.only(right: 8.0, bottom: 5.0),
                child: CampaignCard(),
              ),
          ],
        ),
      ),
    ]);
  }
}
